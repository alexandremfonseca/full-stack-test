<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Rotas de Autenticação
Route::post('register', [UserController::class, 'createUser']);
Route::post('login', [AuthController::class, 'login']);
Route::post('logout', [AuthController::class, 'logout'])
    ->middleware('auth:sanctum');

// Rotas de Usuários
Route::group(['prefix' => 'users', 'middleware' => 'auth:sanctum'], function () {
    Route::get('/', [UserController::class, 'getAllUsers']);
    Route::get('/{pk_usuario}', [UserController::class, 'getUser']);
    Route::put('/{pk_usuario}', [UserController::class, 'updateUser']);
    Route::delete('/{pk_usuario}', [UserController::class, 'deleteUser']);
});

// Rotas de Filmes
Route::group(['prefix' => 'movies', 'middleware' => 'auth:sanctum'], function () {
    Route::get('/', [MovieController::class, 'getAllMovies']);
    Route::get('/{pk_filme}', [MovieController::class, 'getMovie']);
    Route::post('/', [MovieController::class, 'createMovie']);
    Route::put('/{pk_filme}', [MovieController::class, 'updateMovie']);
    Route::delete('/{pk_filme}', [MovieController::class, 'deleteMovie']);
});
