<h1 align="center">🎥 Watch's Full Stack Test</h1>

<p align="center">
 <a href="#about">About</a> • 
 <a href="#tech-stack">Tech Stack</a> •
 <a href="#how-to-run">How to Run</a> •
 <a href="#routes">Routes</a> •
 <a href="#comments">Comments</a> •
 <a href="#contact-me">Author</a>
</p>

## About

🎥 Watch's Full Stack Test é um repositório criado para realização do teste pra vaga de Backend Jr. na Watch Brasil. O sistema consiste de uma API em Laravel com CRUD de usuários e filmes e autenticação em Laravel Sanctum.

## Tech Stack

- **[PHP](https://www.php.net/)**
- **[Laravel](https://laravel.com/)**
- **[MySQL](https://www.mysql.com/)**

## How to Run

1. Clone o projeto:

~~~
git clone https://gitlab.com/alexandremfonseca/full-stack-test.git
~~~

2. Navegue para sua pasta:

~~~
cd full-stack-test
~~~

3. Instale as dependências do projeto:

~~~
composer install
~~~

4. Copie o arquivo `.env.example` e renomeie-o para `.env`:

~~~
cp .env.example .env
~~~

5. Altere as seguintes informações conforme banco de dados de seu ambiente local:
 
~~~
DB_DATABASE=seu_banco_de_dados
DB_USERNAME=seu_usuario
DB_PASSWORD=sua_senha
~~~

6. Migre as tabelas necessárias para o projeto:

~~~
php artisan migrate
~~~

7. Inicie o servidor em Laravel, este pode ser visualizado em http://127.0.0.1:8000/:

~~~
php artisan serve
~~~

## Routes

#### Autenticação
~~~
POST /register
POST /login
POST /logout
~~~

#### Usuários

~~~
GET users/
GET users/{pk_usuario}
PUT users/{pk_usuario}
DELETE users/{pk_usuario}
~~~

#### Filmes

~~~
GET movies/
GET movies/{pk_filme}
POST movies/
PUT movies/{pk_filme}
DELETE movies/{pk_filme}
~~~

## Comments

Devido ao tempo hábil, ao foco da vaga em desenvolvimento Backend e aos poucos conhecimentos em React.js ou Next.js não foi possível realizar o Frontend da aplicação. Dessa forma, foquei na criação da API utilizando boas práticas de código e documentação das funções utilizadas. Após breve estudo das tecnologias de Frontend sugeridas, me chamou a atenção a versatilidade do Next.js, sendo possível utilizá-lo em aplicações Full-Stack, por isso, aumentando meu interesse em aprendê-lo com mais profundidade. Por fim, agradeço a oportunidade de participar do processo!

## Contact Me!

<div>
  <a href="mailto:amfonseca98@gmail.com"><img alt="Gmail" src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white"></a>
  <a href="https://www.linkedin.com/in/alexandremucarzelfonseca/" target="_blank"><img alt="LinkedIn" src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white"></a>
  <a href="https://api.whatsapp.com/send?phone=5541992055294" target="_blank"><img alt="WhatsApp" src="https://img.shields.io/badge/WhatsApp-25D366?style=for-the-badge&logo=whatsapp&logoColor=white"></a>
</div>
