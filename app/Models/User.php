<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Sanctum\HasApiTokens;

class User extends Model
{
    use HasApiTokens, HasFactory;

    protected $table = 'tb_usuarios';

    protected $primaryKey = 'pk_usuario';

    protected $fillable = ['nome', 'email', 'senha'];
}
