<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    use HasFactory;

    protected $table = 'tb_filmes';

    protected $primaryKey = 'pk_filme';

    protected $fillable = ['nome', 'ano_lancamento', 'sinopse', 'capa_jpg'];
}
