<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * Realizar login do usuário
     * @param obj $request => ['email', 'senha']
     * @return json Token de acesso ou mensagem de falha
     */
    public function login(Request $request)
    {
        $loginUserData = $request->validate([
            'email' => 'required|string|email',
            'senha' => 'required|min:8'
        ]);

        $user = User::where('email', $loginUserData['email'])->first();
        if (!$user || !Hash::check($loginUserData['senha'], $user->senha)) {
            return response()->json([
                'success' => false,
                'message' => 'Usuário ou senha incorretos!'
            ]);
        }

        $token = $user->createToken($user->nome . '-AuthToken')->plainTextToken;
        return response()->json([
            'access_token' => $token
        ]);
    }

    /**
     * Realizar logout do usuário
     * @return json Mensagem de sucesso
     */
    public function logout()
    {
        auth()->user()->tokens()->delete();

        return response()->json([
            'success' => true,
            'message' => 'Logout realizado com sucesso!'
        ]);
    }
}
