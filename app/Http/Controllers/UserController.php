<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    /**
     * Retornar todos os usuários
     * @return json Todos os usuários
     */
    public function getAllUsers()
    {
        $users = User::get()->toJson(JSON_PRETTY_PRINT);

        return response($users, 200);
    }

    /**
     * Criar um usuário
     * @param obj $request => ['nome', 'email', 'senha']
     * @return json Mensagem de sucesso 
     */
    public function createUser(Request $request)
    {
        $registerUserData = $request->validate([
            'nome' => 'required|string',
            'email' => 'required|string|email|unique:tb_usuarios',
            'senha' => 'required|min:8'
        ]);

        $user = new User();

        $user->nome = $registerUserData['nome'];
        $user->email = $registerUserData['email'];
        $user->senha = Hash::make($registerUserData['senha']);
        $user->save();

        return response()->json([
            'sucess' => true,
            'message' => 'Usuário criado com sucesso!'
        ], 201);
    }

    /**
     * Retornar um usuário específico
     * @param int $pk_usuario
     * @return json Mensagem de sucesso ou falha
     */
    public function getUser($pk_usuario)
    {
        if (User::where('pk_usuario', $pk_usuario)->exists()) {
            $user = User::where('pk_usuario', $pk_usuario)->get()->toJson(JSON_PRETTY_PRINT);

            return response($user, 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Usuário não encontrado!'
            ], 404);
        }
    }

    /**
     * Atualizar as informações de um usuário específico
     * @param obj $request => ['nome', 'email', 'senha']
     * @param int $pk_usuario
     * @return json Mensagem de sucesso ou falha
     */
    public function updateUser(Request $request, $pk_usuario)
    {
        if (User::where('pk_usuario', $pk_usuario)->exists()) {
            $user = User::find($pk_usuario);

            $updateUserData = $request->validate([
                'nome' => 'nullable|string',
                'email' => 'nullable|string|email',
                'senha' => 'nullable|min:8'
            ]);

            $user->nome = $updateUserData['nome'] ?? $user->nome;
            $user->email = $updateUserData['email'] ?? $user->email;
            $user->senha = Hash::make($updateUserData['senha']) ?? $user->senha;
            $user->save();

            return response()->json([
                'success' => true,
                'message' => 'Usuário atualizado com sucesso!'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Usuário não encontrado!'
            ], 404);
        }
    }

    /**
     * Excluir um usuário específico
     * @param int $pk_usuario
     * @return json Mensagem de sucesso ou falha
     */
    public function deleteUser($pk_usuario)
    {
        if (User::where('pk_usuario', $pk_usuario)->exists()) {
            $user = User::find($pk_usuario);
            $user->delete();

            return response()->json([
                'success' => true,
                'message' => 'Usuário excluído com sucesso!'
            ], 202);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Usuário não encontrado!'
            ], 404);
        }
    }
}
