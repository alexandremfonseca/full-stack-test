<?php

namespace App\Http\Controllers;

use App\Models\Movie;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * Retornar todos os filmes
     * @return json Todos os filmes
     */
    public function getAllMovies()
    {
        $movies = Movie::get()->toJson(JSON_PRETTY_PRINT);

        return response($movies, 200);
    }

    /**
     * Criar um filme
     * @param obj $request => ['nome', 'ano_lancamento', 'sinopse', 'capa_jpg']
     * @return json Mensagem de sucesso
     */
    public function createMovie(Request $request)
    {
        $movie = new Movie();

        $movie->nome = $request->nome;
        $movie->ano_lancamento = $request->ano_lancamento;
        $movie->sinopse = $request->sinopse;
        $movie->capa_jpg = $request->capa_jpg;
        $movie->save();

        return response()->json([
            'success' => true,
            'message' => 'Filme criado com sucesso!'
        ], 201);
    }

    /**
     * Retornar um filme específico
     * @param int $pk_filme
     * @return json Mensagem de sucesso ou falha
     */
    public function getMovie($pk_filme)
    {
        if (Movie::where('pk_filme', $pk_filme)->exists()) {
            $movie = Movie::where('pk_filme', $pk_filme)->get()->toJson(JSON_PRETTY_PRINT);

            return response($movie, 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Filme não encontrado!'
            ], 404);
        }
    }

    /**
     * Atualizar as informações de um filme específico
     * @param obj $request => ['nome', 'ano_lancamento', 'sinopse', 'capa_jpg']
     * @param int $pk_filme
     * @return json Mensagem de sucesso ou falha
     */
    public function updateMovie(Request $request, $pk_filme)
    {
        if (Movie::where('pk_filme', $pk_filme)->exists()) {
            $movie = Movie::find($pk_filme);

            $movie->nome = $request->nome ?? $movie->nome;
            $movie->ano_lancamento = $request->ano_lancamento ?? $movie->ano_lancamento;
            $movie->sinopse = $request->sinopse ?? $movie->sinopse;
            $movie->capa_jpg = $request->capa_jpg ?? $movie->capa_jpg;
            $movie->save();

            return response()->json([
                'success' => true,
                'message' => 'Filme atualizado com sucesso!'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Filme não encontrado!'
            ], 404);
        }
    }

    /**
     * Excluir um filme específico
     * @param int $pk_filme
     * @return json Mensagem de sucesso ou falha
     */
    public function deleteMovie($pk_filme)
    {
        if (Movie::where('pk_filme', $pk_filme)->exists()) {
            $movie = Movie::find($pk_filme);
            $movie->delete();

            return response()->json([
                'success' => true,
                'message' => 'Filme excluído com sucesso!'
            ], 202);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Filme não encontrado!'
            ], 404);
        }
    }
}
